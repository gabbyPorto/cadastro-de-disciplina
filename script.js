let alunos = [];
class Aluno {
  constructor(nome, matricula) {
    this.nome = nome;
    this.matricula = matricula;
  }
}
function cadastrarAluno (){
  let nome = byId("nomeAluno").value;
  let matricula = byId("dis").value;
  let aluno = new Aluno(nome, matricula);
  alunos.push(aluno);
  cadastrarNaDisciplina(); 
} 
 function cadastrarNaDisciplina(){
 let lista = byId("lista");
 let contagem = "<ol>";
for(let aluno of alunos){
  contagem += `<li> ${aluno.nome} (${aluno.matricula}) </il>`
}
 contagem += "</ol>"
lista.innerHTML = contagem;
 }
function byId(id) {
  return document.getElementById(id);
}